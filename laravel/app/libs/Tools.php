﻿<?php

class Tools
{

    public static function formatData($data = null)
    {
        if (!$data) return false;

        $meses = [
            '01' => 'january',
            '02' => 'february',
            '03' => 'march',
            '04' => 'april',
            '05' => 'may',
            '06' => 'june',
            '07' => 'july',
            '08' => 'august',
            '09' => 'september',
            '10' => 'october',
            '11' => 'november',
            '12' => 'december'
        ];

        list($dia, $mes, $ano) = explode('/', $data);
        return $meses[$mes] . ' / ' . $dia . ' / ' . $ano;
    }

    public static function cropText($text = null, $length = null)
    {
        if (!$text || !$length) return false;

        $length = abs((int)$length);

        if(strlen($text) > $length) {
            $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
        }

        return $text;
    }

}