<?php

use \Image, \Input;

class CropImage
{

    public static function make($input, $object)
    {
        if (!Input::hasFile($input) || !$object) {
            return false;
        }

        $image = Input::file($input);
        $name  = date('YmdHis').'_'.$image->getClientOriginalName();

        if (!is_array(array_values($object)[0])) $object = array($object);

        foreach($object as $data) {

            if (!array_key_exists('proportional', $data)) {
                $data['proportional'] = false;
            }

            if (!array_key_exists('upsize', $data)) {
                $data['upsize'] = false;
            }

            $path   = $data['path'].$name;
            $width  = $data['width'];
            $height = $data['height'];

            $upsize = $data['upsize'];

            $imgobj = Image::make($image->getRealPath());

            if ($data['proportional']) {
                $ratio = $imgobj->width() / $imgobj->height();

                if ($ratio > 1) {
                    $imgobj->widen($width, function ($constraint) use ($upsize) {
                    if ($upsize) { $constraint->upsize(); }
                    })->save($path, 100);
                } else {
                    $imgobj->heighten($height, function ($constraint) use ($upsize) {
                    if ($upsize) { $constraint->upsize(); }
                    })->save($path, 100);
                }
            } else {
                $imgobj->fit($width, $height, function ($constraint) use ($upsize) {
                    if ($upsize) { $constraint->upsize(); }
                })->save($path, 100);
            }

            $imgobj->destroy();

        }

        return $name;
    }

}