<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('novidades', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('data');
			$table->string('titulo');
			$table->string('slug')->unique();
			$table->text('texto');
			$table->string('imagem_home');
			$table->string('imagem_novidades');
			$table->string('capa');
			$table->string('embed_site')->default('vimeo');
			$table->string('embed_video');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('novidades');
	}

}
