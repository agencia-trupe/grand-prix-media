<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCabecalhosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cabecalhos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nos');
			$table->string('catalogo');
			$table->string('clientes');
			$table->string('novidades');
			$table->string('contato');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cabecalhos');
	}

}
