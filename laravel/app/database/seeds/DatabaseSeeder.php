<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsuariosSeeder');
		$this->call('ContatoSeeder');
		$this->call('NosSeeder');
		$this->call('CabecalhoSeeder');
	}

}
