<?php

class NosSeeder extends Seeder {

    public function run()
    {
        DB::table('nos')->delete();

        $data = array(
            array(
                'texto' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius voluptatem laboriosam cum modi vero, ipsam adipisci architecto est? Nulla totam, maiores laudantium quas natus illo animi facere libero at iusto. Nulla recusandae, eligendi ad dolore quaerat dolorem, porro distinctio. Voluptatum eligendi iste cum quod fuga atque exercitationem, consequatur, perspiciatis magni quas unde. Iste temporibus excepturi assumenda maiores, quaerat inventore animi dolor perferendis magnam porro officia pariatur, repellendus quo. Sit molestias eaque id, dolorum, odit expedita cupiditate. Autem repellat, laudantium incidunt qui provident corporis dolor ea rem, facere illo totam blanditiis dolorum ullam neque architecto numquam. Enim eligendi non illo, numquam.'
            )
        );

        DB::table('nos')->insert($data);
    }

}