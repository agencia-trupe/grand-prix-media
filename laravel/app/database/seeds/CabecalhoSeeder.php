<?php

class CabecalhoSeeder extends Seeder {

    public function run()
    {
        DB::table('cabecalhos')->delete();

        $data = array(
            array(
                'nos'       => 'placeholder.jpg',
                'catalogo'  => 'placeholder.jpg',
                'clientes'  => 'placeholder.jpg',
                'novidades' => 'placeholder.jpg',
                'contato'   => 'placeholder.jpg'
            )
        );

        DB::table('cabecalhos')->insert($data);
    }

}