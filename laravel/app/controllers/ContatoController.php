<?php

use \Contato;

class ContatoController extends BaseController {

    public function index()
    {
        $contato = Contato::first();
        $contato->telefones = explode(',', preg_replace('/\(([^)]+)\)/', '<span>${1}</span>', $contato->telefones));

        return $this->view('frontend.contato', compact('contato'));
    }

    public function send()
    {
        $contato = Contato::first();

        $nome = Input::get('nome');
        $email = Input::get('email');
        $mensagem = Input::get('mensagem');
        $telefone = Input::get('telefone');
        $telefone = ($telefone ? $telefone : 'Não informado');

        $validation = Validator::make(
            array(
                'nome'     => $nome,
                'email'    => $email,
                'mensagem' => $mensagem
            ),
            array(
                'nome'     => 'required',
                'email'    => 'required|email',
                'mensagem' => 'required'
            )
        );

        if ($validation->fails())
        {
            $response = array(
                'status'  => 'fail',
                'message' => 'Fill out all the fields correctly!'
            );
            return Response::json($response);
        }

        if (isset($contato->email))
        {
            $data = array(
                'nome' => $nome,
                'email' => $email,
                'telefone' => $telefone,
                'mensagem' => $mensagem
            );

            Mail::send('emails.contato', $data, function($message) use ($data, $contato)
            {
                $message->to($contato->email, Config::get('projeto.titulo'))
                        ->subject('[CONTATO] '.Config::get('projeto.titulo'))
                        ->replyTo($data['email'], $data['nome']);
            });
        }

        $object = new ContatoRecebido;
        $object->nome = $nome;
        $object->email = $email;
        $object->telefone = $telefone;
        $object->mensagem = $mensagem;
        $object->save();

        $response = array(
            'status'  => 'success',
            'message' => 'Message sent successfully!'
        );
        return Response::json($response);
    }

}
