<?php

namespace Painel;

use \Novidade, \View, \Input, \Session, \Redirect, \Validator, \Str, \CropImage;

class NovidadesController extends BasePainelController {

    protected $validation_rules = [
        'data'             => 'required|date_format:"d/m/Y"',
        'titulo'           => 'required',
        'texto'            => 'required',
        'imagem_home'      => 'required|image',
        'imagem_novidades' => 'required|image',
        'capa'             => 'image'
    ];

    protected $image_config = [
        'home' => [
            'width'  => 195,
            'height' => 195,
            'path'   => 'assets/img/novidades/home/'
        ],
        'novidades' => [
            'width'  => 675,
            'height' => 200,
            'path'   => 'assets/img/novidades/thumb/'
        ],
        'capa' => [
            'width'  => 750,
            'height' => 500,
            'path'   => 'assets/img/novidades/capa/'
        ]
    ];

    public function index()
    {
        $novidades = Novidade::ordenados()->paginate(10);

        return $this->view('painel.novidades.index', compact('novidades'));
    }

    public function create()
    {
        return $this->view('painel.novidades.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            $input['imagem_home'] = CropImage::make('imagem_home', $this->image_config['home']);
            $input['imagem_novidades'] = CropImage::make('imagem_novidades', $this->image_config['novidades']);
            $input['capa'] = CropImage::make('capa', $this->image_config['capa']);

            Novidade::create($input);
            Session::flash('sucesso', 'Novidade criada com sucesso.');

            return Redirect::route('painel.novidades.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar novidade. Verifique se já existe outra novidade com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $novidade = Novidade::findOrFail($id);

        return $this->view('painel.novidades.edit', compact('novidade'));
    }

    public function update($id)
    {
        $novidade = Novidade::findOrFail($id);
        $input    = Input::all();

        $this->validation_rules['imagem_home'] = 'image';
        $this->validation_rules['imagem_novidades'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem_home')) {
                $input['imagem_home'] = CropImage::make('imagem_home', $this->image_config['home']);
            } else {
                unset($input['imagem_home']);
            }

            if (Input::hasFile('imagem_novidades')) {
                $input['imagem_novidades'] = CropImage::make('imagem_novidades', $this->image_config['novidades']);
            } else {
                unset($input['imagem_novidades']);
            }

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config['capa']);
            } else {
                unset($input['capa']);
            }

            if (Input::has('remover_capa')) {
                $input['capa'] = null;
                unset($input['remover_capa']);
            }

            $input['slug'] = Str::slug(Input::get('titulo'));

            $novidade->update($input);
            Session::flash('sucesso', 'Novidade alterada com sucesso.');

            return Redirect::route('painel.novidades.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar novidade. Verifique se já existe outra novidade com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Novidade::destroy($id);
            Session::flash('sucesso', 'Novidade removida com sucesso.');

            return Redirect::route('painel.novidades.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover novidade.']);

        }
    }

}