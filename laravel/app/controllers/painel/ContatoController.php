<?php

namespace Painel;

use \Contato, \Input, \Session, \Redirect, \Validator;

class ContatoController extends BasePainelController {

    private $validation_rules = [
        'email' => 'required|email'
    ];

    public function index()
    {
    	$contato = Contato::first();

        return $this->view('painel.contato.index', compact('contato'));
    }

    public function update($id)
    {
        $contato = Contato::findOrFail($id);
        $input   = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $contato->update($input);
            Session::flash('sucesso', 'Informações de contato alteradas com sucesso.');

            return Redirect::route('painel.contato.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar informações de contato.'])
                ->withInput();

        }
    }

}