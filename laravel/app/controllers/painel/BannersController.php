<?php

namespace Painel;

use \Banner, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class BannersController extends BasePainelController {

    private $validation_rules = [
        'link'   => 'required',
        'imagem' => 'required|image'
    ];

    public function index()
    {
        $banners = Banner::ordenados()->get();

        return $this->view('painel.banners.index', compact('banners'));
    }

    public function create()
    {
        return $this->view('painel.banners.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', [
                'width'  => 1920,
                'height' => 645,
                'path'   => 'assets/img/banners/'
            ]);
            Banner::create($input);
            Session::flash('sucesso', 'Banner criado com sucesso.');

            return Redirect::route('painel.banners.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar banner.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $banner = Banner::findOrFail($id);

        return $this->view('painel.banners.edit', compact('banner'));
    }

    public function update($id)
    {
        $banner = Banner::findOrFail($id);
        $input  = Input::all();

        $this->validation_rules['imagem'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', [
                    'width'  => 1920,
                    'height' => 645,
                    'path'   => 'assets/img/banners/'
                ]);
            } else {
                unset($input['imagem']);
            }

            $banner->update($input);
            Session::flash('sucesso', 'Banner alterado com sucesso.');

            return Redirect::route('painel.banners.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar banner.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            banner::destroy($id);
            Session::flash('sucesso', 'Banner removido com sucesso.');

            return Redirect::route('painel.banners.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover banner.']);

        }
    }

}