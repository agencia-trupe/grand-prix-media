<?php

namespace Painel;

use \Cabecalho, \Input, \Validator, \Session, \Redirect, \CropImage;

class CabecalhosController extends BasePainelController {


    protected $validation_rules = [
        'nos'       => 'image',
        'catalogo'  => 'image',
        'clientes'  => 'image',
        'novidades' => 'image',
        'contato'   => 'image'
    ];

    protected $image_config = [
        'width'  => 1920,
        'height' => 245,
        'path'   => 'assets/img/cabecalhos/'
    ];

    public function index()
    {
        $cabecalhos = Cabecalho::first();

        return $this->view('painel.cabecalhos.index', compact('cabecalhos'));
    }

    public function update($id)
    {
        $cabecalho = Cabecalho::findOrFail($id);
        $input     = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            foreach ($input as $key => $value) {
                if (Input::hasFile($key)) {
                    $input[$key] = CropImage::make($key, $this->image_config);
                } else {
                    unset($input[$key]);
                }
            }

            $cabecalho->update($input);
            Session::flash('sucesso', 'Imagens alteradas com sucesso.');

            return Redirect::route('painel.cabecalhos.index');

        } catch (\Exception $e) {

            dd($e);

            return Redirect::back()
                ->withErrors(['Erro ao alterar imagens.'])
                ->withInput();

        }
    }

}