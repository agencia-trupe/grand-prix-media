<?php

use \Novidade;

class NovidadesController extends BaseController {

    public function index()
    {
        $novidades = Novidade::ordenados()->paginate(5);

        return $this->view('frontend.novidades.index', compact('novidades'));
    }

    public function show($slug)
    {
        $novidade = Novidade::slug($slug)->first();
        if (!$novidade) App::abort('404');

        View::share('news_title', $novidade->titulo);
        View::share('news_desc', \Tools::cropText(strip_tags($novidade->texto), 120));
        View::share('news_image', url('assets/img/novidades/home/'.$novidade->imagem_home));

        return $this->view('frontend.novidades.show', compact('novidade'));
    }

}
