<?php

use \CatalogoCategoria, \CatalogoFilme;

class CatalogoController extends BaseController {

    public function index($categoria_slug = null)
    {
        $categorias = CatalogoCategoria::ordenados()->get();

        if (!$categoria_slug) {
            $categoria = null;

            $filmes = CatalogoFilme::join('catalogo_categorias', 'catalogo_filmes.categoria_id', '=', 'catalogo_categorias.id')
                ->select('catalogo_filmes.*')
                ->orderBy('catalogo_categorias.ordem', 'ASC')
                ->orderBy('catalogo_filmes.ordem', 'ASC')
                ->with('categoria_parent')
                ->paginate(5);

        } else {
            $categoria = CatalogoCategoria::slug($categoria_slug)->first();
            if (!$categoria) App::abort('404');

            $filmes = CatalogoFilme::categoria($categoria->id)
                ->with('categoria_parent')->ordenados()->paginate(5);
        }

        return $this->view('frontend.catalogo.index', compact('categorias', 'categoria', 'filmes'));
    }

    public function show($categoria_slug, $filme_slug)
    {
        $categoria = CatalogoCategoria::slug($categoria_slug)->first();
        $filme     = CatalogoFilme::slug($filme_slug)->first();

        if (!$categoria || !$filme) App::abort('404');

        return $this->view('frontend.catalogo.show', compact('filme'));
    }

}
