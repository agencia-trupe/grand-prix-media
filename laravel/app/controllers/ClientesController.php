<?php

use \Cliente;

class ClientesController extends BaseController {

    public function index()
    {
        $clientes = Cliente::ordenados()->get();

        return $this->view('frontend.clientes', compact('clientes'));
    }

}
