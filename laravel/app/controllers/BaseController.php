<?php

class BaseController extends Controller {

	protected $layout = 'frontend.common.template';

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    protected function view($path, array $data = [])
    {
        View::share('contato', Contato::first());
        View::share('cabecalho', Cabecalho::first());

        $this->layout->content = View::make($path, $data);
    }

}
