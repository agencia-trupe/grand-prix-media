<?php

use \CatalogoCategoria, \CatalogoFilme, \Novidade;

class HomeController extends BaseController {

	public function index()
	{
        $banners    = Banner::ordenados()->get();
        $novidades  = Novidade::ordenados()->take(3)->get();
        $filmes     = CatalogoFilme::home()->with('categoria_parent')->get();
        $categorias = CatalogoCategoria::ordenados()->get();

		return $this->view('frontend.home', compact('banners', 'novidades', 'filmes', 'categorias'));
	}

}
