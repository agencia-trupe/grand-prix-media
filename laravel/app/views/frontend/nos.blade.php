@section('content')
@include('frontend.common._title', [
    'title' => 'About us',
    'image' => url('assets/img/cabecalhos/'.$cabecalho->nos)
])

    <main id="nos">
        <div class="center">
            <article>
                {{ $nos->texto }}
            </article>

            <aside>
                <h2>Portfolio</h2>
                @if(count($filmes))
                <div class="slideshow">
                    @foreach($filmes as $filme)
                        <img src="{{ url('assets/img/catalogo/poster/'.$filme->poster) }}" title="{{ $filme->titulo }}">
                    @endforeach
                </div>
                @endif
                <a href="{{ route('portfolio') }}">Check the full portfolio<span></span></a>
            </aside>
        </div>
    </main>
@stop