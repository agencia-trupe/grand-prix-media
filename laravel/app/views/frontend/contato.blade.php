@section('content')
@section('content')
@include('frontend.common._title', [
    'title' => 'Contact',
    'image' => url('assets/img/cabecalhos/'.$cabecalho->contato)
])

    <main id="contato">
        <div class="center">
            <aside>
                <p class="telefones">
                @foreach($contato->telefones as $telefone)
                    {{ $telefone }}<br>
                @endforeach
                </p>
                <p class="contato">Contact: {{ $contato->contato }}</p>
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                <p class="endereco">{{ $contato->endereco }}</p>
            </aside>

            <form action="" method="post" id="form-contato">
                    <input type="text" name="nome" id="nome" placeholder="Name" required>
                    <input type="email" name="email" id="email" placeholder="E-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="Phone">
                    <textarea name="mensagem" id="mensagem" placeholder="Message" required></textarea>
                    <input type="submit" value="Send">
                    <div class="resposta-wrapper">
                        <div id="form-resposta"></div>
                    </div>
            </form>
        </div>
    </main>

    <div id="contato-googlemaps">
        {{ $contato->googlemaps }}
    </div>
@stop