    <div id="title" style="background-image: url({{ $image }});">
        <div class="overlay"></div>
        <div class="center">
            <nav id="social">
@if($contato->twitter)
                <a href="{{ $contato->twitter }}" target="_blank" class="twitter">twitter</a>
@endif
@if($contato->facebook)
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook">facebook</a>
@endif
@if($contato->vimeo)
                <a href="{{ $contato->vimeo }}" target="_blank" class="vimeo">vimeo</a>
@endif
@if($contato->youtube)
                <a href="{{ $contato->youtube }}" target="_blank" class="youtube">youtube</a>
@endif
@if($contato->instagram)
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram">instagram</a>
@endif
            </nav>
            <h2>{{ $title }}</h2>
@if(str_is('portfolio', Route::currentRouteName()))
            <div id="dropdown-categorias">
                <a href="#" class="handle">{{ $categoria ? $categoria->titulo : 'Show all' }}</a>
                <ul>
                @if($categoria)
                    <li>{{ link_to_route('portfolio', 'Show all') }}</li>
                @endif
                @foreach($categorias as $cat)
                @if($cat != $categoria)
                    <li>{{ link_to_route('portfolio', $cat->titulo, $cat->slug) }}</li>
                @endif
                @endforeach
                </ul>
            </div>
@endif
        </div>
    </div>