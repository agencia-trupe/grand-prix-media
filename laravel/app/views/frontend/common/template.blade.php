<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Trupe Design">
    <meta name="copyright" content="2015 Trupe Design">

    <meta name="description" content="{{ isset($news_desc) ? $news_desc : Config::get('projeto.description') }}">
    <meta name="keywords" content="{{ Config::get('projeto.keywords') }}">
    <meta property="og:title" content="{{ isset($news_title) ? $news_title : Config::get('projeto.titulo') }}">
    <meta property="og:description" content="{{ isset($news_desc) ? $news_desc : Config::get('projeto.description') }}">
    <meta property="og:site_name" content="{{ Config::get('projeto.titulo') }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:image" content="{{ isset($news_image) ? $news_image : url('assets/img/layout/gpmedia.png') }}">

    <title>{{ isset($news_title) ? $news_title.' | '.Config::get('projeto.titulo') : Config::get('projeto.titulo') }}</title>

    <link rel="stylesheet" href="{{ url('assets/css/main.css') }}">
    <script>var BASE = '{{ url() }}'</script>
</head>
<body>
    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ Config::get('projeto.titulo') }}</a></h1>
            <nav id="desktop">
                <a href="{{ route('about') }}"@if(str_is('about', Route::currentRouteName())) class='active'@endif>about us</a>
                <a href="{{ route('portfolio') }}"@if(str_is('portfolio*', Route::currentRouteName())) class='active'@endif>portfolio</a>
                <a href="{{ route('clients') }}"@if(str_is('clients', Route::currentRouteName())) class='active'@endif>clients</a>
                <a href="{{ route('news') }}"@if(str_is('news*', Route::currentRouteName())) class='active'@endif>news</a>
                <a href="{{ route('contact') }}"@if(str_is('contact', Route::currentRouteName())) class='active'@endif>contact</a>
            </nav>
        </div>

        <nav id="mobile">
            <a href="{{ route('about') }}"@if(str_is('about', Route::currentRouteName())) class='active'@endif>about us</a>
            <a href="{{ route('portfolio') }}"@if(str_is('portfolio*', Route::currentRouteName())) class='active'@endif>portfolio</a>
            <a href="{{ route('clients') }}"@if(str_is('clients', Route::currentRouteName())) class='active'@endif>clients</a>
            <a href="{{ route('news') }}"@if(str_is('news*', Route::currentRouteName())) class='active'@endif>news</a>
            <a href="{{ route('contact') }}"@if(str_is('contact', Route::currentRouteName())) class='active'@endif>contact</a>
        </nav>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>

    @yield('content')
    <footer>
        <div class="center">
            <img src="{{ url('assets/img/layout/gpmedia-footer.png') }}" alt="{{ Config::get('projeto.titulo') }}">
            <p>© {{ date('Y') }} {{ Config::get('projeto.titulo') }} - All rights reserved.</p>
            <p>
                <a href="http://trupe.net" target="_blank">Sites</a>:
                <a href="http://trupe.net" target="_blank" class="kombi">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ url('assets/js/jquery.min.js') }}"><\/script>')</script>
    <script src="{{ url('assets/js/vendor.js') }}"></script>
    <script src="{{ url('assets/js/main.js') }}"></script>
    <script type="text/javascript">
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-61271137-1']);
          _gaq.push(['_trackPageview']);

          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        </script>
</body>
</html>