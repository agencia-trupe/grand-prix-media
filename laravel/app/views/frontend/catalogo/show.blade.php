@section('content')
@include('frontend.common._title', [
    'title' => 'Portfolio',
    'image' => url('assets/img/cabecalhos/'.$cabecalho->catalogo)
])

    <main id="catalogo" class="show">
        <div class="center">
            @if($filme->embed_video)
            <div class="embed">
                <div class="embed-video">
                    @if($filme->embed_site == 'vimeo')
                    <iframe src="//player.vimeo.com/video/{{ $filme->embed_video }}?color=8bc17a&portrait=0&badge=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
                    @elseif($filme->embed_site == 'youtube')
                    <iframe src="http://www.youtube.com/embed/{{ $filme->embed_video }}" frameborder="0"/></iframe>
                    @endif
                </div>
            </div>
            @endif
            <div class="texto">
                <h2>{{ $filme->titulo }}</h2>
                <div class="descricao">{{ $filme->descricao }}</div>
                <div class="ficha-tecnica">{{ $filme->ficha_tecnica }}</div>
                <a href="{{ route('portfolio') }}" class="voltar">back</a>
            </div>
        </div>
    </main>
@stop