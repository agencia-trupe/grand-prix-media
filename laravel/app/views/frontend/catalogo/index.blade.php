@section('content')
@include('frontend.common._title', [
    'title' => 'Portfolio',
    'image' => url('assets/img/cabecalhos/'.$cabecalho->catalogo)
])

    @if(count($filmes))
    <main id="catalogo">
        <div class="center">
            <?php $i=0; ?>
            @foreach($filmes as $filme)
            <?php $i++; ?>
            <a href="{{ route('portfolio.show', [$filme->categoria_parent->slug, $filme->slug]) }}" class="filme-thumb @if($i%2==0) right @endif">
                <div class="gradient"></div>
                <div class="texto">
                    <p class="titulo">{{ $filme->titulo }}</p>
                    <div class="olho">{{ $filme->olho }}</div>
                    <p class="descricao">{{ \Tools::cropText(strip_tags($filme->descricao), 650) }}</p>
                    <span class="botao">+</span>
                </div>
                <img src="{{ url('/assets/img/catalogo/'.$filme->capa) }}" alt="{{ $filme->title }}">
            </a>
            @endforeach
            {{ $filmes->links() }}
        </div>
    </main>

    @else
    <main class="not-found">
        <div class="center">
            <h1>No record in this category</h1>
        </div>
    </main>
    @endif
@stop