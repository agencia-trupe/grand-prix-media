@section('content')
@include('frontend.common._title', [
    'title' => 'News',
    'image' => url('assets/img/cabecalhos/'.$cabecalho->novidades)
])

    <main id="novidades">
        <div class="center">
            @foreach($novidades as $novidade)
            <a href="{{ route('news.show', $novidade->slug) }}" class="novidade-thumb">
                <div class="gradient"></div>
                <div class="texto">
                    <p class="data">{{ \Tools::formatData($novidade->data) }}</p>
                    <p class="titulo">{{ $novidade->titulo }}</p>
                    <p class="texto">{{ \Tools::cropText(strip_tags($novidade->texto), 120) }}</p>
                    <span class="botao">+</span>
                </div>
                <img src="{{ url('assets/img/novidades/thumb/'.$novidade->imagem_novidades) }}" alt="{{ $novidade->title }}">
            </a>
            @endforeach
            {{ $novidades->links() }}
        </div>
    </main>
@stop