@section('content')

    <div id="banner">
        <div class="center">
            <nav id="social">
@if($contato->twitter)
                <a href="{{ $contato->twitter }}" target="_blank" class="twitter">twitter</a>
@endif
@if($contato->facebook)
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook">facebook</a>
@endif
@if($contato->vimeo)
                <a href="{{ $contato->vimeo }}" target="_blank" class="vimeo">vimeo</a>
@endif
@if($contato->youtube)
                <a href="{{ $contato->youtube }}" target="_blank" class="youtube">youtube</a>
@endif
@if($contato->instagram)
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram">instagram</a>
@endif
            </nav>
            <div id="pager"></div>
        </div>
        <div id="banner-slideshow">
            @foreach($banners as $banner)
            <div class="banner-slide" style="background-image: url('{{ url('assets/img/banners/'.$banner->imagem) }}')">
                <div class="overlay"></div>
                <div class="center">
                    <div class="texto-wrapper">
                        <div class="texto">{{ str_replace('&nbsp;', ' ', $banner->texto) }}</div>
                    </div>
                    <a href="{{ $banner->link }}">»</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <main id="home">
        <div class="center">
            @if(count($filmes))
            <div class="trailers">
                <?php $i=0; ?>
                @foreach($filmes as $filme)
                    <?php $i++; ?>
                    <div class="trailer-thumb">
                        <img src="{{ url('assets/img/catalogo/poster/'.$filme->poster) }}" alt="{{ $filme->titulo }}">
                        <a href="{{ route('portfolio.show', [$filme->categoria_parent->slug, $filme->slug]) }}">Trailer<span></span></a>
                        <div class="tooltip @if($i>3) right @endif">
                            <h4>{{ $filme->titulo }}</h4>
                            <div class="olho">{{ $filme->olho }}</div>
                            <p class="descricao">{{ \Tools::cropText(strip_tags($filme->descricao), 400) }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
            @endif

            @if(count($novidades))
            <div class="novidades">
                <h3>News</h3>
                @foreach($novidades as $novidade)
                    <a class="novidade-thumb" href="{{ route('news.show', $novidade->slug) }}">
                        <img src="{{ url('assets/img/novidades/home/'.$novidade->imagem_home) }}" alt="{{ $novidade->titulo }}">
                        <h4>{{ $novidade->titulo }}</h4>
                        <p>{{ \Tools::cropText(strip_tags($novidade->texto), 80) }}</p>
                    </a>
                @endforeach
            </div>
            @endif

            @if(count($categorias))
            <div class="categorias">
                <h3>Portfolio</h3>
                    <ul>
                    @foreach($categorias as $categoria)
                        <li>
                            {{ link_to_route('portfolio', $categoria->titulo, $categoria->slug) }}
                        </li>
                    @endforeach
                    </ul>
            </div>
            @endif
        </div>
    </main>
@stop