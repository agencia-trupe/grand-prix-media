@section('content')

    <legend>
        <h2>Editar Cliente</h2>
    </legend>

    {{ Form::model($cliente, [
        'route' => ['painel.clientes.update', $cliente->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.clientes._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop