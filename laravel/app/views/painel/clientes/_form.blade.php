@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('nome', 'Nome') }}
    {{ Form::text('nome', null, ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('imagem', 'Imagem <small>(250x250px)</small>')) }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/clientes/'.$cliente->imagem) }}" style="display:block; margin-bottom: 10px">
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.clientes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
