@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('olho', 'Olho') }}
    {{ Form::textarea('olho', null, ['class' => 'form-control editor-filme']) }}
</div>

<div class="form-group">
    {{ Form::label('descricao', 'Descrição') }}
    {{ Form::textarea('descricao', null, ['class' => 'form-control editor-filme']) }}
</div>

<div class="form-group">
    {{ Form::label('ficha_tecnica', 'Ficha Técnica') }}
    {{ Form::textarea('ficha_tecnica', null, ['class' => 'form-control editor-filme']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('poster', 'Poster <small>(340x510px)</small>')) }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/catalogo/poster/'.$filme->poster) }}" style="display:block; margin-bottom: 10px">
@endif
    {{ Form::file('poster', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('capa', 'Capa <small>(675x450px)</small>')) }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/catalogo/'.$filme->capa) }}" style="display:block; margin-bottom: 10px">
@endif
    {{ Form::file('capa', ['class' => 'form-control']) }}
</div>

<div class="well">
    <div class="form-group">
        {{ Form::label('embed_site', 'Site do vídeo') }}
        {{ Form::select('embed_site', ['vimeo' => 'Vimeo', 'youtube' => 'YouTube'], null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group" style="margin-bottom:0">
        {{ Form::label('embed_video', 'Código do vídeo') }}
        {{ Form::text('embed_video', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="alert alert-info">
    {{ Form::checkbox('mostrar_home', 'true', null, ['id' => 'mostrar_home']) }}
    {{ Form::label('mostrar_home', 'Selecione para mostrar o poster na home') }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.catalogo.filmes.index', $categoria->id) }}" class="btn btn-default btn-voltar">Voltar</a>