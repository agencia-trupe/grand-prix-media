
@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <a href="{{ route('painel.catalogo.index') }}" class="btn btn-default">&larr; Voltar para Categorias</a>

    <legend>
        <h2>
            <small>Catálogo /</small> {{ $categoria->titulo }}
            <a href="{{ route('painel.catalogo.filmes.create', $categoria->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Filme</a>
        </h2>
    </legend>

    @if(count($filmes))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="catalogo_filmes">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Poster</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($filmes as $filme)

            <tr class="tr-row" id="id_{{ $filme->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ url('assets/img/catalogo/poster/', $filme->poster) }}" alt="" style="max-height:200px; height:auto;"></td>
                <td>{{ $filme->titulo }}</td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => ['painel.catalogo.filmes.destroy', $categoria->id, $filme->id], 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.catalogo.filmes.edit', [$categoria->id, $filme->id]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhum filme cadastrado.</div>
    @endif

@stop