@section('content')

    <legend>
        <h2><small>{{ $categoria->titulo }} /</small> Editar Filme</h2>
    </legend>

    {{ Form::model($filme, [
        'route' => ['painel.catalogo.filmes.update', $categoria->id, $filme->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.catalogo.filmes._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop