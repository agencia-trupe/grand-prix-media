@section('content')

    <legend>
        <h2><small>Catálogo /</small> Adicionar Categoria</h2>
    </legend>

    {{ Form::open(['route' => 'painel.catalogo.store']) }}

        @include('painel.catalogo._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop