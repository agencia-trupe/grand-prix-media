@section('content')

    <legend>
        <h2><small>Catálogo /</small> Editar Categoria</h2>
    </legend>

    {{ Form::model($categoria, [
        'route' => ['painel.catalogo.update', $categoria->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.catalogo._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop