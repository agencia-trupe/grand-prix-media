@section('content')

    {{ Form::open(['route' => 'painel.login', 'class' => 'form-signin']) }}

        <h3 class="form-signin-heading">{{ Config::get('projeto.titulo') }}<br><small>Painel Administrativo</small></h3>

        @if(Session::has('login_errors'))
            <div class="alert alert-danger">Usuário ou Senha inválidos.</div>
        @endif

        {{ Form::text('username', null, ['placeholder' => 'Usuário', 'class' => 'form-control', 'autofocus', 'required']) }}

        {{ Form::password('password', ['placeholder' => 'Senha', 'class' => 'form-control', 'required']) }}

        <div class="checkbox">
            <label>{{ Form::checkbox('lembrar', 'true') }} Lembrar de mim</label>
        </div>

        {{ Form::submit('Login', ['class' => 'btn btn-lg btn-primary btn-block']) }}

    {{ Form::close() }}

@stop