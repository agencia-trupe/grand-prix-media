<?php $login = (Route::currentRouteName() == 'painel.login'); ?>
<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ Config::get('projeto.titulo') }} - Painel Administrativo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/painel.min.css') }}">

    <script>var BASE = '{{ url() }}'</script>
</head>
<body class="{{ ($login ? 'login' : 'painel') }}">
    @if(!$login)
        @include('painel.common.nav')
    @endif

    <div class="{{ ($login ? 'wrapper' : 'main container') }}">
        @yield('content')
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/assets/js/jquery.min.js"><\/script>')</script>
    <script src="{{ url('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('assets/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/js/painel.js') }}"></script>
</body>
</html>