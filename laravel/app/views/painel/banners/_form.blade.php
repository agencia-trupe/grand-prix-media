@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control', 'id' => 'editor-banner']) }}
</div>

<div class="form-group">
    {{ Form::label('link', 'Link') }}
    {{ Form::text('link', null, ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('imagem', 'Imagem <small>(1920x645px)</small>')) }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$banner->imagem) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
