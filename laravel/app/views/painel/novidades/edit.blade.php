@section('content')

    <legend>
        <h2>Editar Novidade</h2>
    </legend>

    {{ Form::model($novidade, [
        'route' => ['painel.novidades.update', $novidade->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.novidades._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop