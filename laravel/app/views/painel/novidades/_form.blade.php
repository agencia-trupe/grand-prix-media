@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('data', 'Data') }}
    {{ Form::text('data', null, ['class' => 'form-control ', 'id' => 'datepicker']) }}
</div>

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control', 'id' => 'editor-novidade']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('imagem_home', 'Imagem na Home <small>(195x195px)</small>')) }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/novidades/home/'.$novidade->imagem_home) }}" style="display:block; margin-bottom: 10px">
@endif
    {{ Form::file('imagem_home', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('imagem_novidades', 'Imagem na Lista de Novidades <small>(675x200px)</small>')) }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/novidades/thumb/'.$novidade->imagem_novidades) }}" style="display:block; margin-bottom: 10px">
@endif
    {{ Form::file('imagem_novidades', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('capa', 'Imagem de Capa <small>(opcional, 750x500px)</small>')) }}
@if($submitText == 'Alterar' && $novidade->capa)
    <img src="{{ url('assets/img/novidades/capa/'.$novidade->capa) }}" style="display:block; margin-bottom: 10px">
    <div class="alert alert-info">
        {{ Form::checkbox('remover_capa', 'true', null, ['id' => 'remover_capa']) }}
        {{ Form::label('remover_capa', 'Selecione para remover imagem de capa') }}
    </div>
@endif
    {{ Form::file('capa', ['class' => 'form-control']) }}
</div>

<div class="well">
    <div class="form-group">
        {{ HTML::decode(Form::label('embed_site', 'Site do vídeo <small>(opcional)</small>')) }}
        {{ Form::select('embed_site', ['vimeo' => 'Vimeo', 'youtube' => 'YouTube'], null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group" style="margin-bottom:0">
        {{ HTML::decode(Form::label('embed_video', 'Código do vídeo <small>(opcional)</small>')) }}
        {{ Form::text('embed_video', null, ['class' => 'form-control']) }}
    </div>
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.novidades.index') }}" class="btn btn-default btn-voltar">Voltar</a>
