@section('content')

    <legend>
        <h2>Adicionar Novidade</h2>
    </legend>

    {{ Form::open(['route' => 'painel.novidades.store', 'files' => true]) }}

        @include('painel.novidades._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop