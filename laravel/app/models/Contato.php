<?php

class Contato extends Eloquent
{

    protected $table = 'contato';

    protected $hidden = [];

    protected $fillable = ['twitter', 'facebook', 'vimeo', 'youtube', 'instagram', 'telefones', 'contato', 'email', 'endereco', 'googlemaps'];

}
