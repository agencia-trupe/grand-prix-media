<?php

class ContatoRecebido extends Eloquent
{

    protected $table = 'contatos_recebidos';

    protected $hidden = [];

    protected $fillable = ['nome', 'email', 'telefone', 'mensagem', 'lido'];

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function scopeNaoLidos($query)
    {
        return $query->where('lido', '!=', 'true');
    }

}
