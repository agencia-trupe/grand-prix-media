<?php

class Cabecalho extends Eloquent
{

    protected $table = 'cabecalhos';

    protected $hidden = [];

    protected $fillable = ['nos', 'catalogo', 'clientes', 'novidades', 'contato'];

}
