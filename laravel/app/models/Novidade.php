<?php

class Novidade extends Eloquent
{

    protected $table = 'novidades';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon\Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

}
