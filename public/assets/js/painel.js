$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	var result = confirm("Deseja Excluir o Registro?");
      	if(result) form.submit();
  	});

    $("table.table-sortable tbody").sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post(BASE + '/painel/ajax/order', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    CKEDITOR.config.contentsCss = BASE + '/assets/css/painel-inputs.css';

    if ($('#editor-nos').length) {
        CKEDITOR.stylesSet.add('nos_styles', [
            { name: 'Destaque', element: 'h3', attributes: { 'class': 'nos-destaque' } }
        ]);

        CKEDITOR.replace('editor-nos', {
            stylesSet: 'nos_styles',
            toolbar: [['Bold', 'Italic'], ['Styles']]
        });
    }

    if ($('#editor-banner').length) {
        var sizes = '', i;

        for(i=32; i <= 150; i+=2) {
            sizes += i+'/'+i+'px;';
        }

        CKEDITOR.config.height = '400px';
        CKEDITOR.config.contentsCss = BASE + '/assets/css/painel-banners.css';
        CKEDITOR.config.colorButton_colors = '4ca6a7,a9cd63';
        CKEDITOR.config.colorButton_enableMore = false;
        CKEDITOR.config.fontSize_sizes = sizes;

        CKEDITOR.replace('editor-banner', {
            toolbar: [['FontSize'], ['TextColor']]
        });
    }

    if ($('#editor-novidade').length) {
        CKEDITOR.replace('editor-novidade', {
            format_tags : 'p;h3',
            toolbar: [['Bold', 'Italic'], ['Format']]
        });
    }

    if ($('.editor-filme').length) {
        $('.editor-filme').each(function (i, obj) {
            CKEDITOR.replace(obj.id, {
                toolbar: [['Bold', 'Italic']]
            });
        })
    }

    if ($('#datepicker').length) {
        $.datepicker.regional['pt-BR'] = {
                closeText: 'Fechar',
                prevText: '&#x3c;Anterior',
                nextText: 'Pr&oacute;ximo&#x3e;',
                currentText: 'Hoje',
                monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
                'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
                'Jul','Ago','Set','Out','Nov','Dez'],
                dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
                dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };

        $.datepicker.setDefaults($.datepicker.regional['pt-BR']);

        $("#datepicker").datepicker();

        if ($('#datepicker').val() == '') {
            $('#datepicker').datepicker("setDate", new Date());
        }
    }

});