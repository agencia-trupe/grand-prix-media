var App = (function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.bannerCycle = function(obj) {
        obj.cycle({
            fx: 'fade',
            slides: '>div',
            pager: '#pager',
            pagerTemplate: '<a href="#"></a>'
        });
    };

    App.bannerSetDesktopText = function() {
        $('.banner-slide span').each(function() {
            var _this = $(this);
            _this.attr('data-size', _this.css('font-size'));
        });
    };

    App.bannerResizeText = function() {
        $('#banner-slideshow').find('.texto span').each(function() {
            var _this = $(this),
                windowWidth = $(window).width();

            if(windowWidth >= 1150) {
                _this.css('font-size', _this.data('size'));
            } else {
                _this.css('font-size', function() {
                    var proportion = ((windowWidth*100) / 1400) / 100,
                        fontsize   = parseInt(_this.data('size')),
                        size       = fontsize * proportion;

                    return (size >= 16 ? size : 16);
                });
            }

            _this.fadeIn();
        });
    };

    App.tooltip = {
        show: function(event) {
            $('.tooltip').hide();

            var tooltip = $(this).parent().find('.tooltip');

            setTimeout(function(event) {
                App.tooltip.move(event, tooltip);
                tooltip.fadeIn('fast');
            }(event), 50);
        },

        move: function(event, tooltip) {
            tooltip = tooltip || $(this).parent().find('.tooltip');
            var parentOffset = tooltip.parent().offset(),
                tooltipX, tooltipY;

            if (tooltip.hasClass('right')) {
                tooltipX = event.pageX - parentOffset.left - 410;
            } else {
                tooltipX = event.pageX - parentOffset.left + 60;
            }

            tooltipY = event.pageY - parentOffset.top - 45;
            tooltip.css({top: tooltipY, left: tooltipX});
        },

        hide: function() {
            var tooltip = $(this).parent().find('.tooltip');

            tooltip.fadeOut('fast');
        }
    };

    App.toggleCategoriasDropdown = function(event) {
        event.preventDefault();
        var $handle = $(this),
            $dropdown = $handle.next('ul');

        $dropdown.slideToggle('fast', function() {
            $handle.toggleClass('active');
        });
    };

    App.toggleMobileNav = function(event) {
        event.preventDefault();

        var $handle = $(this),
            $nav    = $('header nav#mobile');

        $nav.slideToggle();
        $handle.toggleClass('close');
    };

    App.contatoSubmit = function(event) {
        event.preventDefault();

        var $formContato = $(this);
        var $formContatoSubmit = $formContato.find('input[type=submit]');
        var $formContatoResposta = $formContato.find('#form-resposta');

        $formContatoResposta.hide();
        $formContatoSubmit.addClass('loading');

        $.post(BASE + '/contact', {

            nome     : $('#nome').val(),
            email    : $('#email').val(),
            telefone : $('#telefone').val(),
            mensagem : $('#mensagem').val()

        }, function(data) {

            if (data.status == 'success') $formContato[0].reset();

            $formContatoSubmit.removeClass('loading');
            $formContatoResposta.hide().text(data.message).fadeIn('slow');

        }, 'json');
    };

    App.events = function() {
        $('#nos .slideshow').cycle();

        $('#dropdown-categorias .handle').on('click', App.toggleCategoriasDropdown);

        $('#form-contato').on('submit', App.contatoSubmit);

        $('#home .trailer-thumb img').bind({
            mousemove: App.tooltip.move,
            mouseenter: App.tooltip.show,
            mouseleave: App.tooltip.hide
        });

        $(window).resize(App.bannerResizeText);
        App.bannerCycle($('#banner-slideshow'));

        $('#mobile-toggle').on('click touchstart', App.toggleMobileNav);
    };

    App.init = function() {
        App.bannerSetDesktopText();
        App.bannerResizeText();
        App.events();
    };

    return {
        init: App.init
    };

}(window, document, jQuery));

$(document).ready(App.init());