-- MySQL dump 10.13  Distrib 5.6.19-67.0, for Linux (x86_64)
--
-- Host: mysql95.trupe.net    Database: trupe187
-- ------------------------------------------------------
-- Server version	5.6.21-69.0-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'<p><span style=\"font-size:88px\">Screwed</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:34px\">Three friends and a girl adventure out in a Kombi converted into a hot dog stand</span></p>\r\n','http://trupe.net/previa-gpmedia/about','20150325172323_Lascados_Banner.jpg',0,'2015-03-11 17:05:52','2015-03-25 20:23:24'),(3,'<p><span style=\"font-size:88px\">Buddies</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:34px\">A&nbsp;poetic comedy about life through the eyes of three young people with Down syndrome</span></p>\r\n','http://trupe.net/previa-gpmedia/portfolio','20150319145312_colegas.jpg',0,'2015-03-11 17:08:02','2015-03-25 01:04:05'),(4,'<p><span style=\"font-size:88px\">Dirty Hearts</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:34px;\">In Brazil, the war is not over</span></p>\r\n','http://trupe.net/previa-gpmedia/portfolio','20150319145326_coracoes-sujos.jpg',0,'2015-03-11 17:08:35','2015-03-25 00:59:39'),(5,'<p><span style=\"font-size:88px;\">Second Time</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"font-size:34px;\">A middle-aged journalist accidentally turns into a pimp</span></p>\r\n','http://trupe.net/previa-gpmedia/portfolio','20150319145343_segunda-vez.jpg',0,'2015-03-11 17:09:54','2015-03-25 01:00:15'),(6,'<p><span style=\"font-size:88px;\">Carnival of Silence</span><br />\r\n&nbsp;</p>\r\n\r\n<p><span style=\"font-size:34px;\">Carnival meets its true folkloric origin</span></p>\r\n','http://trupe.net/previa-gpmedia/portfolio','20150319145505_carnaval-do-silencio.jpg',0,'2015-03-19 17:55:05','2015-03-25 01:00:48'),(7,'<p><span style=\"font-size:88px\">Mika&#39;s Diary</span></p>\r\n\r\n<p><span style=\"font-size:34px\">Adventures of Mika and her imaginary friends within a tablet</span></p>\r\n','#','20150325134839_gpmedia-oDiarioDeMika.jpg',0,'2015-03-25 16:48:39','2015-03-25 20:39:16');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cabecalhos`
--

DROP TABLE IF EXISTS `cabecalhos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cabecalhos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `catalogo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clientes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `novidades` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cabecalhos`
--

LOCK TABLES `cabecalhos` WRITE;
/*!40000 ALTER TABLE `cabecalhos` DISABLE KEYS */;
INSERT INTO `cabecalhos` VALUES (1,'20150325133506_Besouro.jpg','20150311111130_cabecalho.jpg','20150311111130_cabecalho.jpg','20150311111130_cabecalho.jpg','20150311111130_cabecalho.jpg','0000-00-00 00:00:00','2015-03-25 16:35:06');
/*!40000 ALTER TABLE `cabecalhos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogo_categorias`
--

DROP TABLE IF EXISTS `catalogo_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `catalogo_categorias_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogo_categorias`
--

LOCK TABLES `catalogo_categorias` WRITE;
/*!40000 ALTER TABLE `catalogo_categorias` DISABLE KEYS */;
INSERT INTO `catalogo_categorias` VALUES (1,'Brazilian Film','brazilian-film',0,'2015-03-11 14:27:24','2015-03-17 21:16:19'),(2,'Series','series',2,'2015-03-11 14:27:29','2015-03-17 21:15:39'),(3,'Documentary','documentary',1,'2015-03-11 14:27:35','2015-03-17 17:56:54'),(4,'Musical','musical',3,'2015-03-11 14:27:40','2015-03-17 17:57:30'),(5,'Animation','animation',4,'2015-03-11 14:27:44','2015-03-17 17:57:52');
/*!40000 ALTER TABLE `catalogo_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogo_filmes`
--

DROP TABLE IF EXISTS `catalogo_filmes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo_filmes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `olho` text COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `ficha_tecnica` text COLLATE utf8_unicode_ci NOT NULL,
  `poster` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `embed_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'vimeo',
  `embed_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mostrar_home` tinyint(1) NOT NULL DEFAULT '0',
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `catalogo_filmes_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogo_filmes`
--

LOCK TABLES `catalogo_filmes` WRITE;
/*!40000 ALTER TABLE `catalogo_filmes` DISABLE KEYS */;
INSERT INTO `catalogo_filmes` VALUES (1,1,'Besouro','besouro','<p>Besouro</p>\r\n','<p>Based on the life of a legendary capoeira fighter from Bahia, &quot;Besouro&quot; spins a fantastic tale of a young Brazilian man of African descent in search of his mission.&nbsp;</p>\r\n','<p>Title : Besouro&nbsp;(Original)&nbsp;</p>\r\n\r\n<p>Year production : 2009</p>\r\n\r\n<p>Premiere :October 30, 2009&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 2009&nbsp;95min&nbsp;<br />\r\n<br />\r\nGender :&nbsp;action</p>\r\n\r\n<p>Production: Mixer</p>\r\n','20150311113355_poster1.jpg','20150311113355_filme1.jpg','vimeo','55293867',1,0,'2015-03-11 14:33:55','2015-03-25 23:42:19'),(2,1,'Screwed','screwed','<p>Lascados</p>\r\n','<p>In search of the hectic carnival from Bahia, three friends adventure out in an incredible journey, which never reaches its destination. They travel in a 1970&acute;s VW Kombi, converted into a mobile hot dog stand. The van is their faithful companion in this comedy full of adventures, tales, romance and fun. During the journey they experience a series of unexpected situations and end up helping the runaway daughter of a fearsome tire repairman. The beautiful young girl will test the instincts, the patience and the friendship of the three young travelers.&nbsp;</p>\r\n','<p>Title :&nbsp;Chipped (Original)&nbsp;</p>\r\n\r\n<p>Year production : 2013</p>\r\n\r\n<p>Premiere :&nbsp;September 18, 2014 (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 2013 88min&nbsp;<br />\r\n<br />\r\nGender :Comedy</p>\r\n\r\n<p>Production: Santa Rita Movies</p>\r\n\r\n<p>&nbsp;</p>\r\n','20150325194251_Lascados_Pôster.jpg','20150311113502_filme2.jpg','vimeo','122020826',1,1,'2015-03-11 14:35:02','2015-03-25 23:19:29'),(3,1,'Dirty Hearts','dirty-hearts','<p>Cora&ccedil;&otilde;es Sujos</p>\r\n','<p>In 1945, Japan surrendered to the United States and the Second World War was over. Right? Wrong. For eighty percent of the Japanese community in Brazil, Japan had won the war and defeat was nothing more than American propaganda. The few immigrants that accepted the truth were persecuted. Some were hunted down and assassinated - by their own countrymen - causing the start of a new, private war. Dirty Hearts is a thriller and a love story told by the wife of one of the fanatics dedicated to preach Japanese victory. Little by little, she watches her husband, a hard-working immigrant, become an assassin and their love story fade away.</p>\r\n\r\n<p>&nbsp;</p>\r\n','<p>Title :&nbsp;Dirty Hearts</p>\r\n\r\n<p>Year production : 2014</p>\r\n\r\n<p>Premiere : 2014&nbsp;(Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration: 107min&nbsp;<br />\r\n<br />\r\nGender : Drama</p>\r\n\r\n<p>Production: Mixer</p>\r\n','20150318191926_Dirty_Hearts.jpg','20150318191434_Fogo.JPG','youtube','dbkvcwfxipw',1,2,'2015-03-11 14:35:16','2015-03-25 23:19:41'),(7,1,'Buddies','buddies','<p>Colegas</p>\r\n','<p>Colegas (Buddies) is a road movie that shows the simple things in life in a poetic light through the eyes of three young people with Down syndrome. They work in the video library of an institution where they have always lived in. One day, inspired by the movie &quot;Thelma &amp; Louise&quot;, they decide to run away using the gardener&#39;s old car in order to experience freedom. They travel to different places in search of three simple wishes: Stallone wants to see the ocean, Aninha wants to get married and Marcio wants to fly. During these searches, they embark on several adventures as if life was just a child&#39;s play.&nbsp;</p>\r\n','<p>Title : Colegas &quot;Buddies&quot;&nbsp;(Original)&nbsp;</p>\r\n\r\n<p>Year production :&nbsp;Marcelo Galv&atilde;o</p>\r\n\r\n<p>Premiere :&nbsp;March 1, 2013&nbsp;&nbsp;(Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 2013 94min&nbsp;<br />\r\n<br />\r\nGender :Comedy</p>\r\n\r\n<p>Production:&nbsp;Gatacine</p>\r\n','20150325173326_Colegas_Pôster.jpg','20150325173326_Colegas_Capa.jpg','youtube','6lhtjnDsUpI',1,0,'2015-03-18 22:31:09','2015-03-25 23:17:20'),(15,1,'Farewell','farewell','<p>A Despedida</p>\r\n\r\n<p>&nbsp;</p>\r\n','<p>Based on true facts, &ldquo;Farewell&rdquo; tells the story of Admiral, a 92-year-old man, who decides that the time has come to say goodbye to all that is most important in his life and spends one last night with his lover who is 55 years younger than him. The movie shows that true love is timeless, ageless and limitless.</p>\r\n','<p>Title :&nbsp;A Despedida (Farewell)</p>\r\n\r\n<p>Year production : 2014</p>\r\n\r\n<p>Premiere :&nbsp;October 18&nbsp;2014&nbsp;(Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 2014&nbsp;90min&nbsp;<br />\r\n<br />\r\nGender :Drama</p>\r\n\r\n<p>Production: Gatacine</p>\r\n','20150325173416_Farewell_Pôster.jpg','20150325173416_Farewell_Capa.jpg','vimeo','9953368',1,0,'2015-03-20 20:57:39','2015-03-25 23:17:33'),(16,1,'Noel - The Samba Poet','noel-the-samba-poet','<p>Noel - O poeta da Vila&nbsp;</p>\r\n','<p>The biopic of Noel Rosa, one of the best Brazilian poets and composers.</p>\r\n\r\n<p>&nbsp;At the age of 17, Noel Rosa was a funny young man with a flaw in his chin. He used to study Medicine and play in a local band. He was keen on making friends with workers, poor slum dwellers and prostitutes. One day, he meets Ismael Silva, a composer who challenges him to draw up a samba. Noel, then comes up with a parody of national anthem writing &ldquo;Com que roupa?&rdquo;, which immediately becomes a hit throughout Brazil. From that moment on, Noel starts dedicating himself to the world of samba, turning into one of the most popular composers of the genre.</p>\r\n','<p>Title : Noel - O Poeta da Vila (&nbsp;The Samba Poet)</p>\r\n\r\n<p>Year production : 2006</p>\r\n\r\n<p>Premiere :&nbsp;November 2, 2007&nbsp;(Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration: 1h39m<br />\r\n<br />\r\nGender :Drama</p>\r\n\r\n<p>Production: &nbsp;MOVIE&amp;ART</p>\r\n','20150323114924_Noel 2.jpg','20150323114949_Noel.jpg','vimeo','MOVIE&ART',1,0,'2015-03-20 21:55:43','2015-03-25 23:18:11'),(17,1,'The Pit','the-pit','<p>Rinha</p>\r\n','<p>Based on true stories, The Pit&nbsp;highlights the polemic of bare knuckle fights in a playboy party where drugs and drinks are served on silver trays. This secret posh party is thrown by ex-American school students who bet fortunes in poor wrestlers confronting each other inside an empty swimming pool for a few dollars. Instead of showing poverty in Brazil, The Pit&nbsp;unveils the opposite by unmasking luxurious and sick elite in S&atilde;o Paulo.</p>\r\n','<p>Title : A Rinha (The Pit)&nbsp;Original&nbsp;</p>\r\n\r\n<p>Year production : 2008</p>\r\n\r\n<p>Premiere :&nbsp;2008&nbsp;(Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration: 86min<br />\r\n<br />\r\nGender :Drama</p>\r\n\r\n<p>Production: &nbsp;MOVIE&amp;ART</p>\r\n','20150325173148_Rinha_Pôster.jpg','20150325173148_Rinha_Capa.png','youtube','7saTKyvbLvw',1,0,'2015-03-23 15:44:29','2015-03-25 23:18:48'),(18,1,'400X1','400x1','<p>400 Contra 1</p>\r\n','<p>The real story behind the creation of the Comando Vermelho (Red Commando), the powerful criminal organization that terrorized Rio de Janeiro for several decades.</p>\r\n','<p>Title : 400X1</p>\r\n\r\n<p>Year production : 2010</p>\r\n\r\n<p>Premiere :&nbsp;2010&nbsp;(Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration: 98min<br />\r\n<br />\r\nGender :Drama</p>\r\n\r\n<p>Production: Movie&amp;Art</p>\r\n','20150325174626_400x1 - Pôster.jpg','20150323175809_400-Contra-1-2.jpg','youtube','QBiURRHZcTY',1,0,'2015-03-23 16:09:58','2015-03-25 23:41:26'),(19,3,'Carnival Of Silence','carnival-of-silence','<p>Carnaval do Sil&ecirc;ncio</p>\r\n\r\n<p>&nbsp;</p>\r\n','<p>Carnival is normally associated with a myriad of colours, sounds and rhythms from popular street parties all over Brazil. This documentary, however, goes deep into the sugarcane plantations of the impoverished northeastern region, where Carnival meets its true folkloric origins. Here, it is the caboclo who shines. These are men of mixed indigenous and European&nbsp;descent, who over centuries created their own identity. We explore what it means to be the &ldquo;Caboclo de Lan&ccedil;a&rdquo;, or speared caboclo. A true expression of a rich, yet forgotten popular culture. A silenced Carnival.&nbsp;</p>\r\n','<p>Title : Carnival Of Silence&nbsp;(Original)&nbsp;</p>\r\n\r\n<p>Year production : 2013</p>\r\n\r\n<p>Premiere :&nbsp;2013&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 60min&nbsp;<br />\r\n<br />\r\nGender :&nbsp;Documentary</p>\r\n\r\n<p>Production: Santa Rita</p>\r\n','20150325173514_Carnaval do Silêncio_Pôster-2.jpg','20150323135442_CartazCarnavalSil.jpg','vimeo','c1aa5',1,0,'2015-03-23 16:51:18','2015-03-25 23:20:06'),(21,3,'Going Back','going-back','<p>De Volta</p>\r\n','<p>This fantastic Emmy nominated documentary follows the lives of four inmates who are released to spend only 4 days of freedom with their families and friends during Christmas. In this meantime, they reckon with their past and present.</p>\r\n','<p>Title : Going Back&nbsp;</p>\r\n\r\n<p>Year production : 2013</p>\r\n\r\n<p>Premiere :&nbsp;2013&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 52min&nbsp;<br />\r\n<br />\r\nGender :&nbsp;Documentary</p>\r\n\r\n<p>Production: Futura</p>\r\n','20150325202226_De Volta_Pôster.jpg','20150325202055_De Volta_Capa.jpg','vimeo','107601492',1,0,'2015-03-23 21:16:06','2015-03-25 23:22:27'),(22,5,'Mika´s Diary','mika-s-diary','<p>O Di&aacute;rio de Mika</p>\r\n','<p>An innovative animated series about adventures and discoveries of a 4-year-old girl called Mika and her imaginary friends. Like most modern children, Mika has got a tablet. Being too young to write, she uses it to draw, finding explanations and clarifying her own thoughts. As soon as she starts drawing, her experiences come to life within the tablet. This way she shares the common encounters that every child has with life, feelings and emotions such as fear, anger, curiosity, insecurity, laziness, and others. Mika&rsquo;s Diary will be premiering in Latin American in April, 2015 through Disney Channel, and we have the honor to be representing this amazing new animation around the world!&nbsp;</p>\r\n','<p>Title : Mika&acute;s Diary</p>\r\n\r\n<p>Year production : 2014/2015</p>\r\n\r\n<p>Premiere : 2014/2015</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration: 7m 52 Epis&oacute;dios<br />\r\n<br />\r\nGender :&nbsp;Animation</p>\r\n\r\n<p>Production: &nbsp;Supertoons</p>\r\n','20150325135402_mika-poster.jpg','20150325135402_mika-capa.jpg','vimeo','119363968',1,0,'2015-03-24 23:32:04','2015-03-25 23:35:48'),(23,2,'Second Time ','second-time','<p>Director:</p>\r\n\r\n<p>Cast:</p>\r\n','<p>Raul is a middle-aged journalist who ends up losing his job and wife on the same day.</p>\r\n\r\n<p>One day, by the pool of his new building, he meets many pretty women, but soon finds out that they are all escort girls. Raul then sees himself turning into a pimp in the dangerous and sensual world of high-class prostitution.</p>\r\n','<p>Title :&nbsp;Second Time&nbsp;</p>\r\n\r\n<p>Year production : 2014</p>\r\n\r\n<p>Premiere :&nbsp;2014&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 15min 50&nbsp;Epis&oacute;dios</p>\r\n\r\n<p>&nbsp;Gender :&nbsp;Series</p>\r\n\r\n<p>Production: Conspira&ccedil;&atilde;o</p>\r\n','20150325195219_Second Time_Pôster-1.jpg','20150324211519_Second Time.jpg','youtube','0w1zTW-Eyds',1,0,'2015-03-25 00:15:20','2015-03-25 22:54:30'),(24,3,'Handmade','handmade','<p>Feito a M&atilde;o</p>\r\n','<p>Handmade shows techniques to produce crafts which, besides generating income, value culture. Artisans shwo the step-by-step process to make accessories, household utensils and decorative items made with recycled materials.&nbsp;</p>\r\n','<p>Title :&nbsp;Handmade</p>\r\n\r\n<p>Year production : 2004</p>\r\n\r\n<p>Premiere :&nbsp;2004&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 10/8/26min 27&nbsp;Epis&oacute;dios&nbsp;<br />\r\n<br />\r\nGender :&nbsp;Series</p>\r\n\r\n<p>Production: Futura</p>\r\n','20150325202618_feito 11.PNG','20150325202544_Handmade.jpg','youtube','xWEYSthwEP0',1,0,'2015-03-25 00:27:35','2015-03-25 23:28:52'),(25,5,'The Adventures of Teca ','the-adventures-of-teca','<p>As Aventuras da Teca</p>\r\n','<p>This animation series shows the lives of Teca and her&nbsp;friends. In stories about friendship, playing, companionship,&nbsp;family, personal health and much more. These issues are<br />\r\nalways addressed in a captivating and fun way.</p>\r\n','<p>Title : Besouro&nbsp;(Original)&nbsp;</p>\r\n\r\n<p>Year production : 2012</p>\r\n\r\n<p>Premiere :October 30, 2009&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration: 03min 40&nbsp;Epis&oacute;dios<br />\r\n<br />\r\nGender :&nbsp;Animation</p>\r\n\r\n<p>Production: Futura</p>\r\n','20150325174132_Teca_Pôster-2.jpg','20150325174132_Teca_Capa.jpg','vimeo','107707914',1,0,'2015-03-25 20:41:32','2015-03-25 23:36:03'),(26,3,'Ecological Footprint','ecological-footprint','<p>Pegadas da Ecol&oacute;gia</p>\r\n\r\n<p>Docu-Series</p>\r\n\r\n<p>&nbsp;</p>\r\n','<p>In order to measure the impact of each individual&acute;s consumption on the planet, we invite consumers to follow the same path that a given product travels before it reaches their hands.</p>\r\n','<p>Title :Ecological Footprint</p>\r\n\r\n<p>Year production : 2011</p>\r\n\r\n<p>Premiere :&nbsp;2011&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 26Min<br />\r\n<br />\r\nGender :&nbsp;Documentary</p>\r\n\r\n<p>Production: Futura</p>\r\n','20150325183233_Pegada Ecológica_Pôster.jpg','20150325183234_Pegada Ecológica_Capa.jpg','vimeo','',1,0,'2015-03-25 21:32:34','2015-03-25 23:33:55'),(27,3,'Armed','armed','<p><strong>Armados </strong>/&nbsp;Docu-Series</p>\r\n\r\n<p>Awards&nbsp;</p>\r\n\r\n<p>International New York Film Awards Bronze,2012</p>\r\n\r\n<p>Latin America &amp; Film Studies Association ( LASA) Merit Film,2013</p>\r\n\r\n<p>PromaxBDA Latin American Silver,2012&nbsp;</p>\r\n','<p>The documentary discusses the role of firearms in large urban centers&nbsp;and Brazilian society. It includes testimonies of various sectors that are&nbsp;part of this reality.</p>\r\n','<p>Title : Armed</p>\r\n\r\n<p>Year production : 2013</p>\r\n\r\n<p>Premiere :&nbsp;2013&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 52min&nbsp;<br />\r\n<br />\r\nGender :&nbsp;Documentary</p>\r\n\r\n<p>Production: Futura</p>\r\n','20150325194736_Armados_Pôster.jpg','20150325194736_Armados_Capa.jpg','vimeo','',1,0,'2015-03-25 21:54:11','2015-03-25 23:39:43'),(30,3,'Fair Trade and Solidarity','fair-trade-and-solidarity','<p>Com&eacute;rcio Justo e Solid&aacute;rio</p>\r\n\r\n<p>Docu-Series</p>\r\n','<p>How some people, communities and towns have overcome difficulties and achieved success. This program shows the story of people who manged to access the market and social justice by applying principles of far trade and solidarity.</p>\r\n','<p>Title : Fair Trade and Solidarity</p>\r\n\r\n<p>Year production : 2007</p>\r\n\r\n<p>Premiere :&nbsp;2007&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 26min 10 Epis&oacute;dios&nbsp;<br />\r\n<br />\r\nGender : Series</p>\r\n\r\n<p>Production: Futura</p>\r\n','20150325200318_Comércio_Pôster-2.jpg','20150325200319_Comércio_Capa-1.jpg','vimeo','',1,0,'2015-03-25 23:03:19','2015-03-25 23:33:23'),(31,4,'Encounters in Brazil','encounters-in-brazil','<p>Musical-Series</p>\r\n\r\n<p>Encuentros en Brasil</p>\r\n','<p>Portraying the experience of 6 Latin American musicians who&nbsp;travel across Brazil with a challenge: create a song about the&nbsp;visited destination, record a video clip and experience the<br />\r\ntourism sites of 12 Brazilian cities.&nbsp;A cult reality where Brazil is the main character with its&nbsp;landscapes, its people, its culture and its sounds. In each program&nbsp;the guests have the opportunity to explore unique experiences.&nbsp;Paulinho Moska is the musical director and host to the Latin&nbsp;musicians. The artists of this season are: Jorge Drexler, Kevin&nbsp;Johansen, Francisca Valenzuela, Alejandro and Maria Laura,Andrea Echeverri and Natalia Lafourcade.</p>\r\n','<p>Title :&nbsp;Encounters in Brazil</p>\r\n\r\n<p>Year production : 2014</p>\r\n\r\n<p>Premiere :&nbsp;2014&nbsp; (Brazil)</p>\r\n\r\n<p>Countries of Origin : Brazil</p>\r\n\r\n<p>duration:&nbsp;Brazil, 30min 12&nbsp;Epis&oacute;dios</p>\r\n\r\n<p>&nbsp;Gender :&nbsp;Music-Series</p>\r\n\r\n<p>Production: Santa Rita</p>\r\n','20150325201313_Encontros_Pôster.jpg','20150325204428_04.jpg','vimeo','',1,0,'2015-03-25 23:13:15','2015-03-25 23:44:29');
/*!40000 ALTER TABLE `catalogo_filmes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (2,'MOVIE&ART','20150318211055_small_Movie-&-Art.png',8,'2015-03-11 14:16:57','2015-03-19 00:10:55'),(3,'Supertoons','20150318210235_2.jpg',7,'2015-03-11 14:17:05','2015-03-19 00:02:35'),(4,'SBT','20150318171354_Logo.png',0,'2015-03-11 14:17:24','2015-03-18 20:14:08'),(5,'Canal Futura','20150318171635_1301633_300.jpg',1,'2015-03-11 14:17:39','2015-03-18 20:16:35'),(8,'Conspiração','20150318205853_17-consíra.jpg',4,'2015-03-11 14:18:03','2015-03-18 23:58:53'),(9,'Gatacine','20150318205908_942676_594662330585548_1523628486_n.jpg',5,'2015-03-11 14:18:10','2015-03-18 23:59:08'),(10,'Mixer','20150318205920_images.jpg',6,'2015-03-11 14:18:17','2015-03-18 23:59:20'),(11,'Heco Produções','20150326130342_imagens.png',0,'2015-03-26 16:03:43','2015-03-26 16:04:01'),(12,'Prodigo','20150326130514_C8C4DB5639AA366908CC3D5BB744C824B21FABA0.jpg',0,'2015-03-26 16:05:14','2015-03-26 16:05:14');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vimeo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'','','https://vimeo.com/grandprixmedia','','','(+55 11) 3812 6833, (+55 11) 3814 3958','Grabriel Rohonyi / Chun Kim','comercial@grandprixmedia.net','Av. Magalhães de Castro, 4800 - 18º andar, conj.181 - Edificio Park Tower - Torre II. Cidade Jardim  05676-000, São Paulo - Brazil','<iframe src=\"https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3656.081953939779!2d-46.69835940000004!3d-23.60139360000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sAv.+Magalh%C3%A3es+de+Castro%2C+4800+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1426702679123\" width=\"100%\" height=\"100%\" frameborder=\"0\" style=\"border:0\"></iframe>','0000-00-00 00:00:00','2015-03-18 21:18:22');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
INSERT INTO `contatos_recebidos` VALUES (2,'Teste','teste@teste.com','1234-5678','Lorem ipsum dolor sit amet!','true','2015-03-11 20:47:08','2015-03-17 20:13:36');
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2015_01_28_130645_create_usuarios_table',1),('2015_03_05_135945_create_contato_table',1),('2015_03_05_145031_create_contatos_recebidos_table',1),('2015_03_05_160116_create_nos_table',1),('2015_03_05_170305_create_banners_table',1),('2015_03_05_171429_create_clientes_table',1),('2015_03_06_131519_create_cabecalhos_table',1),('2015_03_06_135612_create_novidades_table',1),('2015_03_06_164838_create_catalogo_categorias_table',1),('2015_03_06_172327_create_catalogo_filmes_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nos`
--

DROP TABLE IF EXISTS `nos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nos`
--

LOCK TABLES `nos` WRITE;
/*!40000 ALTER TABLE `nos` DISABLE KEYS */;
INSERT INTO `nos` VALUES (1,'<p><strong>Grand Prix Media is a company that distributes high quality content to broadcasters and media platforms. Our work encompasses sports rights, content licensing and international channel distribution. From our base in Brazil we serve all of Latin America and through a network of associated companies ensure a global presence.</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','0000-00-00 00:00:00','2015-03-25 22:57:48');
/*!40000 ALTER TABLE `nos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `novidades`
--

DROP TABLE IF EXISTS `novidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `novidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_home` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_novidades` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `embed_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'vimeo',
  `embed_video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `novidades_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `novidades`
--

LOCK TABLES `novidades` WRITE;
/*!40000 ALTER TABLE `novidades` DISABLE KEYS */;
INSERT INTO `novidades` VALUES (1,'2015-03-11','Grand Prix Media celebrates one year in business','grand-prix-media-celebrates-one-year-in-business','<p>Aenean a sagittis tortor. Curabitur dui neque, elementum a eros vel, consequat volutpat sem. Integer id semper urna. In varius pulvinar ligula, a hendrerit turpis. Curabitur sed finibus sapien. Nullam velit ligula, sollicitudin nec ultrices vitae, accumsan sit amet enim. In hac habitasse platea dictumst. Pellentesque non lorem et lorem elementum lacinia. Duis auctor euismod risus, eu dapibus metus. Nam efficitur elit augue, ut condimentum neque rutrum nec. Sed ac libero a purus imperdiet viverra. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse fringilla id dolor vitae hendrerit. Nullam sapien enim, tempor nec magna in, faucibus tristique nisl. Fusce cursus lacus nec enim ultricies, eget scelerisque ante scelerisque.</p>\r\n\r\n<p>Curabitur in hendrerit justo, at scelerisque nunc. Duis rhoncus risus mauris, id laoreet sapien tristique quis. Vivamus semper eget justo eget rhoncus. Nunc congue blandit diam, suscipit pellentesque orci vulputate at. In fringilla dui vel blandit feugiat. Praesent pharetra felis nec ante convallis fringilla. Pellentesque euismod neque ultricies hendrerit ultricies. Proin non nisi vitae libero gravida pulvinar at et nulla. In mattis tellus enim, eget consectetur nisi dignissim vitae. Etiam placerat, odio et convallis aliquet, odio leo sollicitudin est, facilisis elementum elit ante eu urna. Praesent ac quam tortor. Phasellus id augue quis nibh volutpat ultrices quis at neque.</p>\r\n','20150325205713_20130815183828-mipdocbannerlogo.jpg','20150325210650_mipdoc_logo.png','20150325210650_mipdoc_logo.png','vimeo','121450839','2015-03-11 14:22:07','2015-03-26 00:06:50'),(2,'2015-02-28','Praesent placerat gravida pharetra','praesent-placerat-gravida-pharetra','<p>Quisque dictum ligula sit amet rhoncus viverra. Quisque tincidunt velit at elementum tincidunt. Sed sed leo et velit maximus egestas id et eros. Aenean a fringilla turpis, ac pulvinar urna. Suspendisse consectetur tempus tellus nec faucibus.</p>\r\n\r\n<h3>Curabitur vehicula odio a condimentum facilisis</h3>\r\n\r\n<p>Aenean <strong>condimentum </strong>finibus ligula nec interdum. Sed tempus dui in sapien vulputate, in pharetra magna sollicitudin.</p>\r\n','20150311112407_thumb_11.jpg','20150311112407_thumb_11.jpg','0','youtube','VOzw8ViuIws','2015-03-11 14:24:07','2015-03-11 14:24:13'),(3,'2015-01-01','Duis rhoncus risus mauris, id laoreet sapien tristique quis','duis-rhoncus-risus-mauris-id-laoreet-sapien-tristique-quis','<p>Nunc nulla sem, tempus vitae congue sodales, rutrum ut felis. Praesent suscipit nulla eget ex gravida, vitae bibendum magna ultrices. In ut nibh ligula. Sed fringilla nisi at nulla porta, ac sodales ex placerat. Nam at magna eget orci auctor pellentesque. Curabitur nec molestie ex. Donec blandit congue ultrices. Praesent id dolor cursus, volutpat nulla eget, placerat massa. Aenean porta, massa et ultrices venenatis, dolor nisi interdum enim, eget interdum libero arcu in risus. Sed non nulla at urna lobortis semper nec non arcu. Quisque libero ipsum, lobortis nec placerat sed, cursus non lacus.</p>\r\n\r\n<p>Donec nec semper dui. Donec in tincidunt mauris. Aenean in arcu et neque volutpat viverra eu ac erat. Vestibulum mattis magna at lacinia pretium. Curabitur ac bibendum nulla, imperdiet accumsan lorem. Etiam id justo vel lacus euismod aliquet imperdiet sed neque. Maecenas ut molestie est. Praesent ut turpis vel massa vulputate euismod. Vestibulum rutrum diam eu sodales sagittis. Fusce sodales euismod orci, eu dictum mi. Ut mollis elementum cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n\r\n<p>Maecenas sed justo tempus, mattis urna sed, dignissim elit. Aliquam aliquet ligula vel nibh eleifend bibendum. Maecenas malesuada volutpat neque, id consectetur eros faucibus vitae. Aenean ipsum mi, consectetur mollis sagittis quis, consequat sed ipsum. Morbi et felis mi. Sed a augue tempor enim sollicitudin vestibulum. Nulla sodales magna vitae quam blandit, a scelerisque mi tempor. Nunc eu nisl quis justo fringilla elementum. Nunc sem turpis, vestibulum a eros ut, tempus ultrices orci. Donec sodales feugiat enim id ultricies.</p>\r\n','20150311112502_thumb_17.jpg','20150311112502_thumb_17.jpg','0','vimeo','','2015-03-11 14:25:02','2015-03-11 14:25:09'),(4,'2014-12-02','Donec blandit congue','donec-blandit-congue','<p>Donec nec semper dui. Donec in tincidunt mauris. Aenean in arcu et neque volutpat viverra eu ac erat. Vestibulum mattis magna at lacinia pretium. Curabitur ac bibendum nulla, imperdiet accumsan lorem. Etiam id justo vel lacus euismod aliquet imperdiet sed neque. Maecenas ut molestie est. Praesent ut turpis vel massa vulputate euismod. Vestibulum rutrum diam eu sodales sagittis. Fusce sodales euismod orci, eu dictum mi. Ut mollis elementum cursus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n','20150311112539_thumb_23.jpg','20150311112539_thumb_23.jpg','0','vimeo','','2015-03-11 14:25:39','2015-03-11 14:25:39');
/*!40000 ALTER TABLE `novidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'contato@trupe.net','trupe','$2y$10$H/KlUDaT6Kuu8R0.xgvMmu84qsr8ubqEcXH0lOPan00O8nSVz7fWi','JfWnn0I9PVhXpkVZBZ840eZrDTXMryS1d64cFyHwE8fd4K6Kn6ORRf5srOe4','0000-00-00 00:00:00','2015-03-13 14:39:21'),(2,'contato@grandprixmedia.net','gp','$2y$10$Ubr9pyvNx0o8cXq6MVaXtO052hzajL7yc.HGIsbjwvWXC.JkrCInO','','2015-03-13 17:39:45','2015-03-13 17:39:45');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'trupe187'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-26 14:23:50
